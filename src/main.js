// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

import VueFire from 'vuefire'
import firebase from 'firebase/app'
import 'firebase/firestore'

Vue.use(VueFire);


firebase.initializeApp({
  apiKey: "AIzaSyARJifCnpx_L59VlifiYaHgiCQAJA9alSI",
  authDomain: "vue-app-c20f5.firebaseapp.com",
  databaseURL: "https://vue-app-c20f5.firebaseio.com",
  projectId: "vue-app-c20f5",
  storageBucket: "vue-app-c20f5.appspot.com",
  messagingSenderId: "807194474560"

});

export const db = firebase.firestore();

Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: {App},
  template: '<App/>'
});




