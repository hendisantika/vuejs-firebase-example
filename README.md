# vue-app

> VueJS Firebase Example

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).


### Screenshot

SignUp Page

![Sign Up Page](img/signup.png "SignUp Page")

Login Page

![Login Page](img/login.png "Login Page")

Add New Comic

![Add New Comic](img/add.png "Add New Comic")
